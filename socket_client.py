import socket

HOST = "127.0.0.1"
PORT = 65432

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    #s.sendall("Hello, server!".encode("utf-8"))
    while True:
        data_out = s.recv(1024)
        #print(f"recived data: {data_out.decode('utf-8')}")
        data_in = input("Input text: ").encode("utf-8")
        if not data_in:
            break
        s.sendall(data_in)
